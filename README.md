# Face-Age-genDer-Emotion (FADE) detection

## Prerequisite

- CMake: for dlib package

## Recommend Development package

- pytz
- pytest
- flake8