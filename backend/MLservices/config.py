# application configuration
import os
from enum import Enum
from starlette.config import Config
from pydantic import PostgresDsn, validator
from typing import Dict, Any, Optional


class Settings():

    # Load Starlette Configuration
    # Read from environment variable
    config = Config()
    # Storage Configuration
    USER_STORAGE: str = config('USER_STORAGE')
    USER_IMAGE_DIRNAME: str = config('USER_IMAGE_DIRNAME')
    USER_VIDEO_DIRNAME: str = config('USER_VIDEO_DIRNAME')
    USER_MODEL_DIRNAME: str = config('USER_MODEL_DIRNAME')

    IMAGE_PREFIX: str = config('IMAGE_PREFIX')
    VIDEO_PREFIX: str = config('VIDEO_PREFIX')

    # Celery
    CELERY_WORKER_NAME: str = config('CELERY_WORKER_NAME')
    MESSAGE_BROKER: str = config('MESSAGE_BROKER')

    # Database
    POSTGRES_SERVER: str = config('POSTGRES_SERVER')
    POSTGRES_USER: str = config('POSTGRES_USER')
    POSTGRES_PASSWORD: str = config('POSTGRES_PASSWORD')
    POSTGRES_DB: str = config('POSTGRES_DB')
    SQLALCHEMY_DATABASE_URI: Optional[PostgresDsn] = None

    # TFServer
    RESTfulURL: str = config('RESTful_URL')
    ageURLstr: str = config('ageURL')
    emotionURLstr: str = config('emotionURL')
    faceURLstr: str = config('faceURL')
    genderURLstr: str = config('genderURL')

    @classmethod
    def tfserverurl(cls, modelurl):
        return f'{cls.RESTfulURL}{modelurl}'

    @property
    def ageurl(self):
        return self.tfserverurl(self.ageURLstr)
    @property
    def emotionurl(self):
        return self.tfserverurl(self.emotionURLstr)
    @property
    def faceurl(self):
        return self.tfserverurl(self.faceURLstr)
    @property
    def genderurl(self):
        return self.tfserverurl(self.genderURLstr)

    @validator("SQLALCHEMY_DATABASE_URI", pre=True)
    def assemble_db_connection(cls, v: Optional[str], values: Dict[str, Any]) -> Any:
        if isinstance(v, str):
            return v
        return PostgresDsn.build(
            scheme="postgresql",
            user=values.get("POSTGRES_USER"),
            password=values.get("POSTGRES_PASSWORD"),
            host=values.get("POSTGRES_SERVER"),
            path=f"/{values.get('POSTGRES_DB') or ''}",
        )

    # Model configuration
    STORAGE_PATH: str = config('STORAGE_PATH')
    STATIC_MODEL_STORAGE: str = config('STATIC_MODEL_STORAGE')
    XML_FACE_CASCADE: str = config('XML_FACE_CASCADE')
    AGE_MODEL: str = config('AGE_MODEL')
    GENDER_MODEL: str = config('GENDER_MODEL')
    EMOTION_MODEL: str = config('EMOTION_MODEL')
    FACE_POS_DEPLOY_TXT: str = config('FACE_POS_DEPLOY_TXT')
    FACE_POS_WEIGHT: str = config('FACE_POS_WEIGHT')
    FACENET_WEIGHT: str = config('FACENET_WEIGHT')
    FACE_LANDMARK_MODEL: str = config('FACE_LANDMARK_MODEL')

    @property
    def get_url(self):
        user = self.POSTGRES_USER
        password = self.POSTGRES_PASSWORD
        server = self.POSTGRES_SERVER
        db = self.POSTGRES_DB
        return f'postgresql://{user}:{password}@{server}/{db}'


settings = Settings()


# Enumerator

# Upload Configuation
class AllowedType(Enum):
    @classmethod
    def has(cls, value):
        return value in cls._value2member_map_


class AllowImageType(str, AllowedType):
    jpg = 'jpg'
    jpeg = 'jpeg'
    png = 'png'
    dotjpg = '.jpg'
    dotjpeg = '.jpeg'
    dotpng = '.png'


class AllowVideoType(str, AllowedType):
    mp4 = 'mp4'
    dotmp4 = '.mp4'
