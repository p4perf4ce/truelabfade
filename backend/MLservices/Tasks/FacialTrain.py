import celery
from loguru import logger
from app.crud import traintask
from app.db.session import SessionLocal
from app.schemas import TrainTaskUpdate


class Task(celery.Task):

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        state = 'FAILED'
        logger.error('{0!r} {1!r}: {2!r}'.format(task_id, state, exc))
        # Run database update
        try:
            db = SessionLocal()
            task = traintask.get_by_key(db=db, key=task_id)
            schema = TrainTaskUpdate(
                state=state,
                meta='{0!r}'.format(exc),
            )
            if task:
                traintask.update(db=db, db_obj=task, obj_in=schema)
        finally:
            db.close()


    def on_success(self, retval, task_id, args, kwargs):
        state = 'SUCCESS'
        logger.success('{0!r} {1!r}'.format(task_id, state))
        try:
            db = SessionLocal()
            task = traintask.get_by_key(db=db, key=task_id)
            schema = TrainTaskUpdate(
                state=state,
                meta='Successfully trained'
            )
            if task:
                traintask.update(db=db, db_obj=task, obj_in=schema)
        finally:
            db.close()
