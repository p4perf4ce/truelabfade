from pathlib import Path

from loguru import logger as log

from config import settings

import logging

from tenacity import (
    after_log, before_log, retry, stop_after_attempt, wait_fixed)

from app.db.session import SessionLocal

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

max_tries = 60 * 5  # 5 minutes
wait_seconds = 1


@retry(
    stop=stop_after_attempt(max_tries),
    wait=wait_fixed(wait_seconds),
    before=before_log(logger, logging.INFO),
    after=after_log(logger, logging.WARN),
)
def init() -> None:
    try:
        # Try to create session to check if DB is awake
        db = SessionLocal()
        db.execute("SELECT 1")
    except Exception as e:
        logger.error(e)
        raise e


def main() -> None:
    logger.info("Initializing service")
    init()
    logger.info("Service finished initializing")


if __name__ == "__main__":
    main()


if __name__ == '__main__':
    # Run initialize script
    logger.info("Pre-starting worker ...")
    logger.info("Checking model storage ...")
    storage = Path(settings.STORAGE_PATH)
    modelStorage = Path(settings.STATIC_MODEL_STORAGE)
    facenet = Path(settings.FACENET_WEIGHT)
    age_w = Path(settings.AGE_MODEL)
    gender_w = Path(settings.GENDER_MODEL)
    emotion_w = Path(settings.EMOTION_MODEL)
    dpath = [storage, modelStorage]
    fpath = [facenet, age_w, gender_w, emotion_w]
    log.info(f"Storage {str(storage)}: -> {storage.is_dir()}")
    log.info(f"modelStorage {str(modelStorage)}: -> {modelStorage.is_dir()}")
    log.info(f"facenet {str(facenet)}: -> {facenet.is_file()}")
    log.info(f"age {str(age_w)}: -> {age_w.is_file()}")
    log.info(f"Storage {str(gender_w)}: -> {gender_w.is_file()}")
    log.info(f"Storage {str(emotion_w)}: -> {emotion_w.is_file()}")
    for p in dpath:
        assert p.is_dir()
    for p in fpath:
        assert p.is_file()
    main()
