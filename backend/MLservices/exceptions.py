class LabelNotExists(Exception):
    """
    Try to train non-existing data with such a label
    """
    def __init__(self, label: str):
        self.label = label
