import cv2


def get_output(label, pos, image):
    X, Y, endX, endY = pos

    # Blue color in BGR
    color = (255, 0, 0)
    thickness = 2
    font = cv2.FONT_HERSHEY_SIMPLEX
    fontScale = 1
    image = cv2.rectangle(image, (X, Y), (endX, endY), color, thickness)

    image = cv2.putText(image, label, (X, Y-5), font,
                        fontScale, color, thickness, cv2.LINE_AA)

    return image


class ProgressTracker:
    state: str = None
    info: str = None

    def __repr__(self):
        return f"<Progress(state='{self.state}', \
            info='{self.info}', \
            meta={self.meta})>"

    @classmethod
    def set_state(cls, _state):
        cls.state = _state

    @classmethod
    def set_info(cls, _info):
        cls.info = _info

    @classmethod
    def set_meta(cls, _meta):
        cls.meta = _meta
