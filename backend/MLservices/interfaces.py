# Services internal API
from pathlib import Path
from typing import List
import traceback

from loguru import logger

from .celery_app import celery_app
from .config import settings  # noqa: F401

from .services import (
    face_recognition, predict_age, predict_emotion, predict_gender)

from .model.FaceRecog import train, loader
from .utils import ProgressTracker
from .Tasks.FacialTrain import Task


def face_recog_pattern(key) -> Path:
    return Path(f'facial-recognition-instance-{key}')


def face_recog_model_template(key) -> Path:
    return Path(f'representations-{key}.pkl')


@celery_app.task()
def test_celery(word: str = 'Stella') -> str:
    return f"Heartbeating: {word}"


@celery_app.task(bind=True, max_retires=3)
def recognize_face(
    self,
    image_path: Path,
    representations_file: Path,
    output_image: Path,
    max_similar: int = 5
):
    try:
        img, faces_dict = face_recognition(
                            image_path=image_path,
                            representations_file=representations_file,
                            output_image_path=output_image,
                            max_similar=max_similar)
    except Exception as e:
        return e
    return img, faces_dict


@celery_app.task(bind=True, acks_late=True, base=Task)
def train_facial_recognition(
    self,
    key: str,
    source_video_storage: str,
    labels: List[str],
    output_path: str,
):
    """
    key: str -> uuid Instance identifier
    source_video_storage: Path -> User's video upload storage
    labels: label needed to train
    output_path <- usually a User's userStorage
            |- facerecog-instance-{key}
                    |- Processed
                    |- Model
                        |- representations-{key}.pkl
    NOTE:
    - For each expected long running task's can be created as new subtask
    and called asynchronously.
    - Redis, RPC or Cassandra backend should be used
      instead of writing data to db.
    """
    # Initializing progress tracker
    progress_tracking = ProgressTracker()
    progress_tracking.set_state('INITIALIZE')
    progress_tracking.set_info('Initializing task.')
    meta = {
        'parent': None,
        'child': None,
        'value': 0,
        'info': dict()}
    progress_tracking.set_meta(meta)
    self.update_state(
        state='INITIAL',
        meta={
            'info': 'INITIALIZING',
            'task': None,
            'value': 0,
            'meta': None
        }
    )
    source_video_storage = Path(source_video_storage)
    output_path = Path(output_path)  # userStorage/username
    # Prepare output directory
    dirpath = output_path.joinpath(face_recog_pattern(key))  # ../username/instancedir/
    try:
        dirpath.mkdir()
    except FileExistsError as e:
        logger.error(f'dirpath {str(dirpath)} already existed.')
        self.update_state(
            state='FAILURE',
            meta={
                'exc_type': type(e).__name__,
                'exc_message': traceback.format_exc().split('\n'),
                'info': 'Directory existed.',
                'task': None,
                'value': 0,
                'meta': None,
            }
        )
        raise e

    Extracted_dir = dirpath.joinpath('Processed')
    Model_dir = dirpath.joinpath('Model') # ../../instancedir/Model
    Extracted_dir.mkdir()
    Model_dir.mkdir()
    Modelpath = Model_dir.joinpath(face_recog_model_template(key))
    try:
        assert not Modelpath.is_file()
    except AssertionError as e:
        self.update_state(
            state='FAILURE',
            meta={
                'exc_type': type(e).__name__,
                'exc_message': traceback.format_exc().split('\n'),
                'info': 'Cannot make path for modelfile.',
                'task': None,
                'value': 0,
                'meta': None
            }
        )
        raise e
    progress_tracking.set_state('LOADING')
    progress_tracking.set_info('Loading raw data')
    self.update_state(
        state='LOADING',
        meta={
            'info': 'Loading raw data',
            'task': 'Initializing',
            'value': 0,
            'meta': None
        }
    )
    # Process data
    progress = 0
    for data in loader(
        labels=labels,
        source_dir=source_video_storage,
        output_path=Extracted_dir,
        use_existed=False,
    ):
        try:
            if data['status'] != 'done':
                progress = data['index']/data['total']
                self.update_state(
                    state='PROCESSING',
                    meta={
                        'info': 'Extracting face from video',
                        'task': 'Extracting',
                        'value': round(progress, 1)*75,
                        'meta': data
                    }
                )
        except Exception as e:
            self.update_state(
                state='FAILURE',
                meta={
                    'exc_type': type(e).__name__,
                    'exc_message': traceback.format_exc().split('\n'),
                    'info': 'Cannot make path for modelfile.',
                    'task': 'Extracting',
                    'value': round(progress, 1)*75,
                }
            )
            raise e
    self.update_state(
        state='TRAINING',
        meta={
            'info': 'Training facial recognition',
            'task': 'Training',
            'value': 75,
            'meta': None
        }
    )
    # Train model
    train(
        label_storage=Extracted_dir,
        labels=labels,
        output_model_path=Modelpath,
    )
    self.update_state(
        state='SUCCESS',
        meta={
            'info': 'Training facial recognition',
            'task': 'Training',
            'value': 100,
            'meta': None
        }
    )
    # Profit
    return 0


@celery_app.task()
def age_prediction(
    target_path: Path
):
    logger.info(f'target_path: {target_path}')
    result = predict_age(image_path=target_path)
    logger.info(f'age_task: {result}')
    return result


@celery_app.task()
def emotion_detection(
    target_path: Path
):
    result = predict_emotion(image_path=target_path)
    logger.info(f'emotion_task: {result}')
    return result


@celery_app.task()
def gender_detection(
    target_path: Path
):
    result = predict_gender(image_path=target_path)
    logger.info(f'gender_task: {result}')
    return result
