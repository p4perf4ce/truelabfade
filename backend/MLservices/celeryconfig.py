# Celery extra config

# Writing extended result to backend (Celery don't handle migrations !)
result_extended = True

# Database backend table schemas
database_table_names = {
    'task': 'mlworker_lonemeta',
    'group': 'mlworker_groupmeta',
}
