from celery import Celery

from .config import settings
from . import celeryconfig


result_backend = settings.get_url

celery_app = Celery(
    settings.CELERY_WORKER_NAME,
    broker=settings.MESSAGE_BROKER,
    backend=f"db+{result_backend}"
)
celery_app.config_from_object(celeryconfig)
# All task routes register here

prefix = 'MLservices.interfaces'
main_queue = 'main-queue'
predict_queue = 'predict-queue'
train_queue = 'train-queue'
# config queue on scaling.
celery_app.conf.task_routes = {
    f"{prefix}.test_celery": main_queue,
    f"{prefix}.recognize_face": main_queue,
    f"{prefix}.train_facial_recognition": main_queue,
    f"{prefix}.age_prediction": main_queue,
    f"{prefix}.emotion_detection": main_queue,
    f"{prefix}.gender_detection": main_queue,
    }
