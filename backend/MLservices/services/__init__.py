from .age_predict import predict_age            # noqa: F401
from .emotion_predict import predict_emotion    # noqa: F401
from .face_recog import face_recognition        # noqa: F401
from .gender_predict import predict_gender      # noqa: F401
