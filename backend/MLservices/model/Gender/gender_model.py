from tensorflow.keras import Model
from tensorflow.keras.layers import (
    Convolution2D, Dense, Input, Flatten,
    Dropout, MaxPooling2D, BatchNormalization,
    GlobalMaxPool2D, Concatenate, GlobalMaxPooling2D,
    GlobalAveragePooling2D, Lambda, Conv2D
)
from tensorflow.keras.applications.mobilenet_v2 import MobileNetV2


def get_gender_class():
    gender_class = {0: 'Female',
                    1: 'Male'}
    return gender_class