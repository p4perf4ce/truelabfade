from tensorflow.keras import Model
from tensorflow.keras.layers import (
    Convolution2D, Dense, Input, Flatten,
    Dropout, MaxPooling2D, BatchNormalization,
    GlobalMaxPool2D, Concatenate, GlobalMaxPooling2D,
    GlobalAveragePooling2D, Lambda, Conv2D
)
from tensorflow.keras.applications.mobilenet_v2 import MobileNetV2


def get_age_model(weight_path):

    base_model = MobileNetV2(weights='imagenet',
                             include_top=False,
                             input_shape=(224, 224, 3))

    x = base_model.output
    x = GlobalMaxPooling2D()(x)
    x = Dropout(0.5)(x)
    x = Dense(128, activation="relu")(x)
    x = Dropout(0.5)(x)
    x = Dense(8, activation="softmax")(x)

    base_model = Model(base_model.input, x, name="base_model")
    base_model.compile(
        loss="categorical_crossentropy",
        metrics=['acc'],
        optimizer="adam")
    base_model.load_weights(weight_path)

    return base_model


def get_age_class():
    age_class = {0: '0-10',
        1: '11-20',
        2: '21-30',
        3: '31-40',
        4: '41-50',
        5: '51-60',
        6: '61-70',
        7: '71-100'}
    return age_class
