import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import (
    Convolution2D, Dense, Input, Flatten,
    Dropout, MaxPooling2D, BatchNormalization,
    GlobalMaxPool2D, Concatenate, GlobalMaxPooling2D,
    GlobalAveragePooling2D, Lambda, Conv2D
)
from tensorflow.keras.optimizers import Adam


def get_emotion_class():
    emotion_class = {0: 'Uncertain',
                     1: 'angry',
                     2: 'disgusted',
                     3: 'fearful',
                     4: 'happy',
                     5: 'neutral',
                     6: 'sad',
                     7: 'surprised'}
    return emotion_class


def get_emotion_model(weightpath):
    model = Sequential()
    model.add(
        Conv2D(32, kernel_size=(3, 3),
               activation='relu', input_shape=(48, 48, 1)))
    model.add(Conv2D(64, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(128, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Conv2D(128, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(1024, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(8))
    # model.add(Dense(8, activation='softmax'))
    model.add(tf.keras.layers.Softmax())

    model.load_weights(weightpath)
    model.compile(loss='sparse_categorical_crossentropy',
                  optimizer=Adam(lr=0.001, decay=1e-6), metrics=['accuracy'])
    return model
