import os
import cv2
import dlib
import numpy as np

from MLservices.config import settings

detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(settings.FACE_LANDMARK_MODEL)


def get_face_landmark(image_path: str):
    locations = []
    im = cv2.imread(str(image_path))
    dets = detector(im, 1)
    for det in dets:
        shape = predictor(im, det)
        for idx in range(shape.num_parts):
            point = shape.part(idx).x, shape.part(idx).y
            locations.append(point)
    return locations


def detect_face(img_path: str):
    image = cv2.imread(img_path)
    locations = get_face_landmarks(img_path)
    net = cv2.dnn.readNetFromCaffe(settings.FACE_POS_DEPLOY_TXT,
                                   settings.FACE_POS_WEIGHT)

    (h, w) = image.shape[:2]
    blob = cv2.dnn.blobFromImage(cv2.resize(image, (300, 300)), 1.0,
                                 (300, 300), (104.0, 177.0, 123.0))

    net.setInput(blob)
    detections = net.forward()

    face_index = []
    for i in range(0, detections.shape[2]):
        confidence = detections[0, 0, i, 2]
        if confidence > 0.75:
            face_index.append(i)
    pos = []
    age_gender_faces = np.empty((len(face_index), 224,
                                 224, 3))
    emotion_faces = np.empty((len(face_index), 224,
                              224, 3))
    # faces = np.empty((len(face_index), 160, 160, 3i))
    faces = np.empty((len(face_index), 112, 112, 3))
    for i in face_index:
        box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
        (startX, startY, endX, endY) = box.astype("int")
        # y = startY - 10 if startY - 10 > 10 else startY + 10
        pos.append((startX, startY, endX, endY))

        f_img = image[startY:endY, startX:endX, :]

        age_gender_faces[i, :, :, :] = cv2.resize(f_img, (224, 224))
        emotion_faces[i, :, :, :] = age_gender_faces[i, :, :, :]
        age_gender_faces[i, :, :, :] = age_gender_faces[i, :, :, :]/255.0

        faces[i, :, :, :] = cv2.resize(f_img, (112, 112))
        faces[i, :, :, :] = faces[i, :, :, :]/255.0
    return len(faces), emotion_faces, age_gender_faces, faces, pos, locations
