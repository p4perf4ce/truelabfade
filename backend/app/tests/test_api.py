import pytest

from fastapi.testclient import TestClient
from app.main import app
from app.core.config import API_KEY
from app.core.models.beatcheck import Beat


# Global test variable
header = {"token": str(API_KEY)}


# Factory function
def factory_test_api(url: str):
    f = open('tests/test_img/lisa.jpg', 'rb')
    test_files = {
        "file": (
            "lisa.jpg",
            f,
            "image/jpeg")
        }
    try:
        with TestClient(app) as client:
            resp = client.post(
                url=url,
                headers=header,
                files=test_files,
                )
    finally:
        f.close()
    assert resp.status_code == 200


# TEST CASE GOES HERE
def test_check_beat():
    with TestClient(app) as client:
        resp = client.get("/api/beat")
    assert resp.status_code == 200
    assert resp.json() == Beat(is_alive=True)


@pytest.mark.skip(reason='not implemented')
def test_recog_face():
    pass


def test_predict_age():
    factory_test_api("/api/predict/age")


def test_predict_gender():
    factory_test_api("/api/predict/gender")


def test_predict_emotion():
    factory_test_api("/api/predict/emotion")
