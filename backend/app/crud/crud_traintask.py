import os

from typing import List


from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app.crud.base import CRUDBase
from app.models.train_task import TrainTask
from app.schemas.train_task import TrainTaskCreate, TrainTaskUpdate


class CRUDTrainTask(CRUDBase[TrainTask, TrainTaskCreate, TrainTaskUpdate]):
    """
    CRUD class for item (user's file)
    """
    def create_with_owner(
        self, db: Session, *, obj_in: TrainTaskCreate, owner_id: int
    ) -> TrainTask:
        """
        Create Item with ItemCreate Schema.
        """
        obj_in_data = jsonable_encoder(obj_in)
        db_obj = self.model(**obj_in_data, owner_id=owner_id)
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def get_multi_by_owner(
        self, db: Session, *, owner_id: int, skip: int = 0, limit: int = 100
    ) -> List[TrainTask]:
        """
        Return Items' from x with total of y belonging to the user.
        """
        # Other filter now must done on the frontend side.
        return (
            db.query(self.model)
            .filter(TrainTask.owner_id == owner_id)
            .offset(skip)
            .limit(limit)
            .all()
        )

    def get_by_key_owner(
        self, db: Session, *, owner_id: int, key: str
    ) -> TrainTask:
        return (
            db.query(self.model)
            .filter(TrainTask.owner_id == owner_id,
                    TrainTask.model_id == key)
            .first()
        )

    def get_by_taskid_owner(
        self, db: Session, *, owner_id: int, key: str
    ) -> TrainTask:
        return (
            db.query(self.model)
            .filter(TrainTask.owner_id == owner_id,
                    TrainTask.task_id == key)
        ).first()

    def get_by_key(
        self, db: Session, *, key: str,
    ) -> TrainTask:
        """
        key: str Celery's Taskid
        """
        return (
            db.query(self.model)
            .filter(TrainTask.task_id == key)
            .first()
        )

    def update(self, db: Session, *,
               db_obj: TrainTask, obj_in: TrainTaskUpdate
               ) -> TrainTask:
        if isinstance(obj_in, dict):
            update_data = obj_in
        else:
            update_data = obj_in.dict(exclude_unset=True)
        if update_data['state']:
            # do something
            pass
        return super().update(db, db_obj=db_obj, obj_in=update_data)

    def delete_by_key_owner(
        self, db:Session, *, owner_id:int, key: str,
    ):
        query = db.query(self.model).filter(
            TrainTask.owner_id == owner_id,
            TrainTask.model_id == key)
        # Delete local model
        if query is None or query.first() is None:
            return;
        model_path = query.first().modelpath
        if os.path.isfile(model_path):
            os.remove(model_path)
        query.delete()
        db.commit()



traintask = CRUDTrainTask(TrainTask)
