import os
import shutil
from pathlib import Path
from typing import Any, List
from uuid import uuid4

from loguru import logger
from fastapi import File, Form, UploadFile

from app.core.config import AllowImageType, AllowVideoType, settings
from app.core.exceptions import UnallowedFormat
from app.crud import item

#
# NOTE: To limit
# File Upload sizes, should be done by nginx or traefik.
# Otherwise, we need to read file in chunk, which is not a good option.


def ext_extract(filename: str) -> str:
    """
    extract ext, `.mp4` still get mp4 and valid.
    """
    ext = filename.split('.')[-1]
    return ext


def name_gen_order(prefix: str, ext: str, order: any) -> str:
    name = prefix + '-' + str(order) + '.' + ext
    return name


def name_gen_uuid(prefix: str, ext: str) -> str:
    lateral = str(uuid4())
    name = prefix + '-' + lateral + '.' + ext
    return name


def write_path(path_dir: Path, filename: str) -> Path:
    full_path = os.path.join(
        path_dir,
        filename
    )
    return full_path


async def upload_image(
    file: UploadFile = File(...),
    save_path: Path = Path()
) -> Path:
    """
    Single image file upload. Just put it somewhere, maybe user dir.
    Params:
    file: UploadFile object
    save_path: Image destination path
    """
    ext: str = ext_extract(file.filename)
    # Check suffixes
    if not AllowImageType.has(ext):
        raise UnallowedFormat(ext=ext)
    try:
        # Write from mem to disk
        img_path = save_path
        with img_path.open('wb') as buffer:
            shutil.copyfileobj(file.file, buffer)
    finally:
        logger.debug("Write image to {}".format(img_path))
        file.file.close()
    return img_path


async def upload_videos(
    files: List[UploadFile] = File(...),
    label: str = str(...),
    save_dirpath: Path = Path(),
) -> (Path, int):
    """
    Validate multiple video files upload and storage handling
    files: List of UploadFile objects
    label: video label
    save_dirpath: storage directory
    """
    files_count = len(files)
    files_ext = [ext_extract(file.filename) for file in files]
    logger.debug("User Uploading {} files".format(files_count))
    for ext in files_ext:
        if not AllowVideoType.has(ext):
            raise UnallowedFormat(ext=ext)

    videoupload_dir = save_dirpath
    labelvideo_dir = videoupload_dir.joinpath(label)
    logger.debug("Writing {} files to disk at {}".format(files_count,
                                                         labelvideo_dir))
    if os.path.isdir(labelvideo_dir):
        # Assume delete all files
        shutil.rmtree(labelvideo_dir)
    # Create directory
    os.mkdir(labelvideo_dir)
    assert Path(labelvideo_dir).is_dir()
    for index, file in enumerate(files):
        try:
            _video_path = Path(write_path(labelvideo_dir,
                                          name_gen_order(
                                            settings.VIDEO_PREFIX, ext, index
                                            )))
            with _video_path.open('wb') as _buffer:
                shutil.copyfileobj(file.file, _buffer)
                logger.debug("Write video to {}".format(_video_path))
        finally:
            file.file.close()
    return labelvideo_dir, files_count


async def delete_videos(save_dirpath: Path, label: str):
    labelvideo_dir = save_dirpath.joinpath(label)
    if os.path.isdir(labelvideo_dir):
        shutil.rmtree(labelvideo_dir)
