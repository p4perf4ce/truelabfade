from typing import TYPE_CHECKING

from sqlalchemy import (
    Boolean, Column, Integer, String)
from sqlalchemy.orm import relationship

from app.db.base_class import Base

if TYPE_CHECKING:
    from .item import Item  # noqa: F401


class User(Base):
    """
    DECLARATION:
    Username should only contain:
        - alphanumerical character a-zA-Z0-9
    Keep in mind that in "any" case username should not contain
    `@` character.
    """
    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, index=True, nullable=False)
    email = Column(String, unique=True, index=True, nullable=False)
    full_name = Column(String, index=True)
    hashed_password = Column(String, nullable=False)
    is_active = Column(Boolean(), default=True)
    is_superuser = Column(Boolean(), default=False)
    storage = Column(String, unique=True)
    items = relationship("Item", back_populates="owner")
    traintasks = relationship("TrainTask", back_populates="owner")

    def __repr__(self):
        return "<User(id={},\
                username='{}',\
                email='{}'\
                name='{}',\
                is_active={},\
                is_admin={},\
                storage='{}')>"\
                .format(self.id,
                        self.username,
                        self.email,
                        self.full_name,
                        self.is_active,
                        self.is_superuser,
                        self.storage)
