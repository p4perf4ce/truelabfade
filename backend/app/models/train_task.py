import json

from typing import TYPE_CHECKING

from sqlalchemy import (
    Column, ForeignKey, String, Integer)
from sqlalchemy.orm import relationship

from app.db.base_class import Base

if TYPE_CHECKING:
    from .user import User  # noqa: F401


class TrainTask(Base):
    """
    task_id: Celery's task-id, given when send task to queue.
    model_id: fastapi generated key.
    state: Model's state tell model availability
    instancepath: Model's training instance directory
    modelpath = Model's weight file path
    labels = Labels trained.
    owner_id = Model's owner id.
    owner = postgresql relationship.
    """
    id = Column(Integer, primary_key=True, index=True)
    task_id = Column(String, index=True, unique=True)
    model_id = Column(String, index=True, unique=True)
    state = Column(String)
    meta = Column(String)
    instancepath = Column(String)
    modelpath = Column(String)
    labels = Column(String)
    owner_id = Column(Integer, ForeignKey('user.id'))
    owner = relationship("User", back_populates="traintasks")

    @property
    def labeldict(self):
        return json.loads(self.labels)

    def __repr__(self):
        return "<TrainingTask(task_id='{}', owner_id='{}')>"\
            .format(self.task_id, self.owner_id)
