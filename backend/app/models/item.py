import enum
from typing import TYPE_CHECKING

from sqlalchemy import (
    Column, ForeignKey, Integer, String, Enum)
from sqlalchemy.orm import relationship

from app.db.base_class import Base

if TYPE_CHECKING:
    from .user import User  # noqa: F401


class Item(Base):
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True)
    path = Column(String)
    owner_id = Column(Integer, ForeignKey('user.id'))
    owner = relationship("User", back_populates="items")

    def __repr__(self):
        return "<Item(name='{}', type='{}', owner_id='{}')>"\
            .format(self.name, self.item_type, self.owner_id)
