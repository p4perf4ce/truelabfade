# App server
import logging

from fastapi import FastAPI, Request, status
from fastapi.responses import JSONResponse

from starlette.responses import RedirectResponse

# Import API
from app.api.routes import api_router

# Import Event Handler
from app.core.event_handlers import startup_handler

# Import Exceptions
from app.core.exceptions import (
                                    UnallowedFormat,
                                    UploadFailure,
                                )
from app.core.config import settings

# Logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def create_app() -> FastAPI:

    server = FastAPI(title=settings.APP_NAME, openapi_url='/api/openapi.json')

    # register api
    server.include_router(prefix='/api', router=api_router)

    # add event handlers
    server.add_event_handler("startup", startup_handler(server))

    # custom exceptions
    @server.exception_handler(UnallowedFormat)
    async def unallow_format_handler(req: Request, exc: UnallowedFormat):
        return JSONResponse(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            content={"message": f"{exc.ext} file type unacceptable."}
        )

    @server.exception_handler(UploadFailure)
    async def upload_failure_handler(req: Request, exc: UploadFailure):
        return JSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={"message": "Upload procedure unexpectedly fail."}
        )

    @server.get("/", include_in_schema=False)
    def root() -> RedirectResponse:
        # root GET for root redirect to documentation.
        logger.debug(msg="GET / Redirecting")
        response = RedirectResponse(url='/docs')
        return response

    return server


app = create_app()
