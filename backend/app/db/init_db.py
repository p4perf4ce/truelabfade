from pathlib import Path

from sqlalchemy.orm import Session

from app import crud, schemas
from app.core.config import settings

from app.db import base  # noqa: F401
# All models must be called before initialization with sqlalchemy
# Otherwise SQLAlchemy can't establish relationships


def init_db(db: Session) -> None:
    # Initialize default admin
    user = crud.user.get_by_username(
        db, username=settings.FIRST_SUPERUSER
    )
    # If not exists, create such a user
    if not user:
        user_in = schemas.UserCreate(
            username=settings.FIRST_SUPERUSER,
            email=settings.FIRST_SUPERUSER_EMAIL,
            password=settings.FIRST_SUPERUSER_PASSWORD,
            is_superuser=True,
            storage=str(Path(settings.USER_STORAGE).joinpath(settings.FIRST_SUPERUSER))
        )
        try:
            Path(user_in.storage).mkdir()
        except FileExistsError:
            pass
        user = crud.user.create(db, obj_in=user_in)
