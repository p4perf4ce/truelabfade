# get all inheritance to Declarative base
# for SQA to create table

from app.db.base_class import Base  # noqa:F401
from app.models.item import Item  # noqa:F401
from app.models.user import User  # noqa:F401
from app.models.train_task import TrainTask  # noqa:F401
