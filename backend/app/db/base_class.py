from typing import Any

from sqlalchemy.ext.declarative import (
    as_declarative, declared_attr)


# Get subclass (from /models)
@as_declarative()
class Base:
    id: Any
    __name__: str

    @declared_attr
    def __tablename__(cls) -> str:
        # tablename generator
        return cls.__name__.lower()
