from fastapi import APIRouter

from app.api.endpoints import (
    is_alive, mlservices, upload, auth)
from app.core.config import settings


api_router = APIRouter()

@api_router.post('/')
def api_root():
    return {'msg': '/api/'}

# register API
api_router.include_router(
    is_alive.router, tags=['check-alive']
)

api_router.include_router(
    mlservices.router, tags=['predict']
)

api_router.include_router(
    upload.router, tags=['upload']
)

api_router.include_router(
    prefix=settings.AUTH_BASE_STR,
    router=auth.router, tags=['authenticate']
)
