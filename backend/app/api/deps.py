# Depends

from typing import Generator
from uuid import UUID

from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from jose import jwt
from pydantic import ValidationError
from sqlalchemy.orm import Session

from app import crud, models, schemas
from app.core import security
from app.core.config import settings
from app.db.session import SessionLocal


reusable_oauth2 = OAuth2PasswordBearer(
    tokenUrl='/api{}{}'.format(
        settings.AUTH_BASE_STR,
        settings.ACCESS_TOKEN_STR)
)


def get_db() -> Generator:
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


def get_current_user(
    db: Session = Depends(get_db),
    token: str = Depends(reusable_oauth2),
) -> models.User:
    """
    Validate JWT and retrive user from db
    """
    try:
        payload = jwt.decode(
            token, settings.SECRET_KEY,
            algorithms=[security.ALGORITHM]
        )
        token_data = schemas.TokenPayload(**payload)
    except (jwt.JWTError, ValidationError):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail='Could not validate credentials.'
        )
    # user id should be in jwt `sub` part
    user = crud.user.get(db, id=token_data.sub)

    if not user:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail='User not found.'
        )
    return user


def validate_key(key: str, version: int = 4):
    try:
        return UUID(key).version == version
    except ValueError:
        return None


def get_current_active_user(
    curr_user: models.User = Depends(get_current_user)
) -> models.User:
    if not crud.user.is_active(curr_user):
        raise HTTPException(
            status_code=status.HTTP_423_LOCKED,
            detail='User Inactivated.'
        )
    return curr_user


def get_current_active_superuser(
    curr_user: models.User = Depends(get_current_user),
) -> models.User:
    if not crud.user.is_superuser(curr_user):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail='Not enough privilage.'
        )
    return curr_user
