from datetime import timedelta
from typing import Any

from fastapi import (
    APIRouter, Depends, Form, HTTPException, status)
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from app import crud, models, schemas
from app.api import deps
from app.core import security
from app.core.config import settings


router = APIRouter()


@router.post(settings.ACCESS_TOKEN_STR,
             response_model=schemas.Token)
def access_token(
    db: Session = Depends(deps.get_db),
    form_data: OAuth2PasswordRequestForm = Depends()
) -> Any:
    """
        Login and get free cookie.
        (Access token)
    """
    user = crud.user.authenticate(
        db,
        username=form_data.username,
        password=form_data.password
    )
    if not user:
        # Check for client id and client secret instead
        valid_clientId = security\
                     .key_comparer(
                         form_data.client_id,
                         settings.CLIENT_ID)
        valid_clientSecret = security\
            .key_comparer(
                form_data.client_secret,
                settings.CLIENT_SECRET)
        if valid_clientId and valid_clientSecret:
            user = crud.user.get_by_username(
                db=db,
                username=settings.FIRST_SUPERUSER
            )
        else:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail='Incorrect username or password.'
            )
    elif not crud.user.is_active(user):
        raise HTTPException(
            status_code=status.HTTP_423_LOCKED,
            detail='User Inactivated.'
        )
    access_token_expires = timedelta(
        minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES
    )
    return {
        "access_token": security.create_access_token(
            subject=user.id,
            expires_delta=access_token_expires
        ),
        'token_type': 'bearer',
    }


@router.post(settings.CHECK_TOKEN_STR, response_model=schemas.User)
def test_token(
    current_user: models.User = Depends(deps.get_current_user)
) -> Any:
    """
    Test access token
    """
    return current_user


@router.post(settings.PASS_RESET_VIA_CLIENT_STR, response_model=schemas.Msg)
def reset_password_via_client(
    db: Session = Depends(deps.get_db),
    form_data: OAuth2PasswordRequestForm = Depends(),
    new_password: str = Form(...)
) -> Any:
    valid_clientId = security\
                     .key_comparer(
                         form_data.client_id,
                         settings.CLIENT_ID)
    valid_clientSecret = security\
        .key_comparer(
            form_data.client_secret,
            settings.CLIENT_SECRET)

    if valid_clientId and valid_clientSecret:
        user = crud.user.get_by_username(
            db=db,
            username=settings.FIRST_SUPERUSER)
        if not user:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail='User does not exists.'
            )
        elif not crud.user.is_active(user):
            raise HTTPException(
                status_code=status.HTTP_423_LOCKED,
                detail='User Inactivated'
            )
    return {'msg': 'Password changed.'}
