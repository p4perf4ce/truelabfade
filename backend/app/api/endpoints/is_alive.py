from fastapi import APIRouter

from app.schemas import Beat

router = APIRouter()


@router.get("/beat", response_model=Beat, name="beatcheck")
def check_alive() -> Beat:
    beat = Beat(is_alive=True)
    return beat
