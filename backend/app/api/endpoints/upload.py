# For batch uploading
import os
from pathlib import Path
from typing import List

from fastapi import (
    APIRouter, Depends, UploadFile, File, Form, status)
from fastapi.responses import HTMLResponse, JSONResponse
from fastapi.exceptions import HTTPException
from loguru import logger
from sqlalchemy.orm import Session

from app import models
from app.api import deps
from app.core.config import settings
from app.schemas import UploadVideoResp
from app.core.exceptions import UploadFailure
from app.crud.utils import Userupload

router = APIRouter()


@router.post("/uploadvid", tags=['uploadvid'])
async def upload_video(
    db: Session = Depends(deps.get_db),
    curr_user: models.User = Depends(deps.get_current_active_user),
    files: List[UploadFile] = File(...),
    label: str = Form(...)
        ):
    logger.info(f"User: {curr_user.username} uploads label {label}")
    save_path = Path(curr_user.storage)\
        .joinpath(settings.USER_UPLOAD_DIR)\
        .joinpath(settings.USER_VIDEO_DIRNAME)

    if not save_path.is_dir():
        try:
            save_path.mkdir()
        except FileExistsError:
            pass
        except Exception:
            try:
                os.makedirs(str(save_path))
            except Exception as e:
                logger.error(f'Unable to make upload directory. {e}')
                raise UploadFailure
    try:
        video_dir, video_count = await Userupload\
            .upload_videos(
                files=files,
                save_dirpath=save_path,
                label=label,
                )
    except Exception as e:
        logger.error(f'Unable to processed upload')
        raise e
    return UploadVideoResp(
            label=label,
            total=video_count)


@router.delete("/deletevid/{video_label}")
async def delete_videos_with_label(
    video_label: str,
    db: Session = Depends(deps.get_db),
    curr_user: models.User = Depends(deps.get_current_active_user),
    ):
    save_path = Path(curr_user.storage)\
        .joinpath(settings.USER_UPLOAD_DIR)\
        .joinpath(settings.USER_VIDEO_DIRNAME)
    await Userupload.delete_videos(save_path, video_label)
    return 200


@router.get("/uploadvid", tags=['uploadvid'])
async def upload_video_form(
    curr_user: models.User = Depends(deps.get_current_active_user)
):
    """
    Mockup upload form, on productionn just embed token somewhere.
    """
    content = f"""
    <body>
    <h1>USER: {curr_user.username}<h1>
    <form enctype="multipart/form-data" method="post">
    <label for="uploadsel">Videos upload (allowed extension: .mp4)</label><br>
    <br><input id="uploadsel" name="files" type="file" multiple required><br>
    <br><label for="uploadlabel">label</label>
    <input id="uploadlabel" name="label" type="text" required><br><br>
    <input type="submit" value="Upload">
    </form>
    </body>
    """

    return HTMLResponse(content=content)
