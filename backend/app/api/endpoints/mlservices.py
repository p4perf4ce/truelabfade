# NOTE:
# FastAPI has File and UploadFile
# File will stays on the memory as bytes.
# UploadFile will stays on the memory as a spool,
# which will write to disk after spoolmemory are exceeded.
import json
from pathlib import Path
from uuid import uuid4
import shutil
from typing import List

from celery.result import AsyncResult
from fastapi import APIRouter, Depends, UploadFile, File, Form, status
from fastapi.exceptions import HTTPException
from fastapi.responses import JSONResponse
from loguru import logger
from sqlalchemy.orm import Session

from app.core.config import settings
from app import crud, models, schemas  # noqa: F401
from app.api import deps
from app.crud.utils import upload_image

from MLservices.interfaces import (
    recognize_face,
    age_prediction,
    gender_detection,
    emotion_detection,
    train_facial_recognition,
    face_recog_model_template,
    face_recog_pattern
)


from app.schemas import (
                         FaceRecog,
                         AgePrediction,
                         GenderDetection,
                         EmotionDetection,
                         TaskProgress,
                         ModelReport
                )

router = APIRouter()


# Helper function
def userUploadpathImage(
    userStorage: str,
    prefix: str,
    suffix: str,
) -> Path:
    """
    Params:
    user: user model
    """
    path = Path(userStorage)
    # check user storage
    if not path.is_dir():
        path.mkdir()
    # get user upload dir
    uppath = path.joinpath(settings.USER_UPLOAD_DIR)
    if not uppath.is_dir():
        uppath.mkdir()
    # get upload image dir
    uppath = uppath.joinpath(settings.USER_IMAGE_DIRNAME)
    if not uppath.is_dir():
        uppath.mkdir()
    filename = f'{prefix}-{str(uuid4())}.{suffix}'
    uppath = uppath.joinpath(filename)
    return uppath


def listvideo_label(
    storage: Path,
):
    try:
        labels = [label.stem for label in storage.iterdir() if label.is_dir()]
    except FileNotFoundError:
        labels = []
    return labels

# Routes


# Face Recognition
@router.post("/facial/recognition",
             response_model=FaceRecog,
             tags=["predict"])
async def face_recognition(
    db: Session = Depends(deps.get_db),
    curr_user: models.User = Depends(deps.get_current_active_user),
    file: UploadFile = File(...),
    model_id: str = Form(...)
        ) -> FaceRecog:
    logger.info("Face recognition task running")
    _suffix = Path(file.filename).suffix
    image_path = await upload_image(
        file=file,
        save_path=userUploadpathImage(
            curr_user.storage,
            settings.IMAGE_PREFIX,
            _suffix,
        )
    )
    assert Path(image_path).is_file
    query = crud.traintask.get_by_key_owner(db=db, owner_id=curr_user.id, key=model_id)
    if query is None:
        modelpath = ''
    modelpath = query.modelpath
    modelfile = Path(modelpath)
    try:
        assert modelfile.is_file()
    except AssertionError:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail={'msg': 'No model found'}
        )
    _, result = recognize_face(
        image_path=str(image_path),
        representations_file=modelfile,
        output_image=userUploadpathImage(
            curr_user.storage,
            'face-recog',
            _suffix,
        )
    )
    resp = FaceRecog(faces_name=result, detail=dict())
    return resp


# Age Prediction
@router.post("/facial/age",
             response_model=AgePrediction, tags=["predict"])
async def age_predict(
    curr_user: models.User = Depends(deps.get_current_active_user),
    file: UploadFile = File(...)
        ) -> AgePrediction:
    """
    Age prediction from image.
    """
    _suffix = Path(file.filename).suffix
    logger.info("Age prediction task running")
    image_path = await upload_image(
            file=file,
            save_path=userUploadpathImage(
                curr_user.storage,
                settings.IMAGE_PREFIX,
                _suffix,
            )
        )
    taskresult = age_prediction.apply(kwargs={'target_path': image_path})
    result, result_detail = taskresult.get(timeout=1)
    taskresult.forget()
    return AgePrediction(age=result, detail=result_detail)


# Gender Prediction
@router.post("/facial/gender",
             response_model=GenderDetection, tags=["predict"])
async def gender_detect(
    curr_user: models.User = Depends(deps.get_current_active_user),
    file: UploadFile = File(...)
        ) -> GenderDetection:
    """
    Gender prediction from image
    """
    _suffix = Path(file.filename).suffix
    image_path = await upload_image(
            file=file,
            save_path=userUploadpathImage(
                curr_user.storage,
                settings.IMAGE_PREFIX,
                _suffix,
            )
        )
    taskresult = gender_detection.apply(kwargs={'target_path': image_path})
    result, result_detail = taskresult.get(timeout=1)
    taskresult.forget()
    resp = GenderDetection(gender=result, detail=result_detail)
    return resp


# Emotion Detection
@router.post("/facial/emotion",
             response_model=EmotionDetection, tags=["predict"])
async def emotion_detect(
    curr_user: models.User = Depends(deps.get_current_active_user),
    file: UploadFile = File(...)
        ) -> EmotionDetection:
    """
    Emotion detection from image.
    """
    _suffix = Path(file.filename).suffix
    image_path = await upload_image(
            file=file,
            save_path=userUploadpathImage(
                curr_user.storage,
                settings.IMAGE_PREFIX,
                _suffix,
            )
        )
    taskresult = emotion_detection.apply(kwargs={'target_path': image_path})
    result, result_detail = taskresult.get(timeout=1)
    taskresult.forget()
    resp = EmotionDetection(emotion=result, detail=result_detail)
    return resp


# Train
def get_label_from_dir(userStoragePath):
    storage_path = Path(userStoragePath)\
        .joinpath(settings.USER_UPLOAD_DIR)\
        .joinpath(settings.USER_VIDEO_DIRNAME)
    labels = listvideo_label(storage_path)
    return storage_path, labels


@router.get("/facial/labels", tags=['facial-recog'])
def list_available_label(
    curr_user: models.User = Depends(deps.get_current_active_user),
):
    """
    List all available labels
    """
    _, labels = get_label_from_dir(curr_user.storage)

    return JSONResponse(status_code=status.HTTP_200_OK,
                        content={'labels': labels})


@router.delete("/facial/labels", tags=['facial-recog'])
def delete_label(
    curr_user: models.User = Depends(deps.get_current_active_user),
    labels: List[str] = Form(...),
):
    extract_labels = []
    [
        [ extract_labels.append(_l) for _l in l.split(',') ]
        for l in labels
    ]
    storage_path, userLabels = get_label_from_dir(curr_user.storage)
    for l in extract_labels:
        if l in userLabels:
            toDelete = storage_path.joinpath(l)
            try:
                shutil.rmtree(str(toDelete))
            except Exception as e:
                logger.error(f'Error deleting label: {l}, {type(e).__name__}')
    return 200


@router.get("/facial/models/", tags=['facial-recog'])
def list_model(
    db: Session = Depends(deps.get_db),
    curr_user: models.User = Depends(deps.get_current_active_user),
    skip: int = 0,
    limit: int = 20,
):
    """
    Check user's models, return from 0 to 19th model by default.
    """
    data = list()

    models = crud.traintask.get_multi_by_owner(
        db=db, owner_id=curr_user.id, skip=skip, limit=limit)
    for model in models:
        data.append(ModelReport(
            task_id=model.task_id,
            model_id=model.model_id,
            labels=model.labeldict,
            state=model.state,
            meta=model.meta).todict
        )

    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=data
    )


@router.get("/facial/models/{model_id}", tags=['facial-recog'])
def list_model(
    model_id: str,
    db: Session = Depends(deps.get_db),
    curr_user: models.User = Depends(deps.get_current_active_user),
):
    """
    Check user's model by model-id
    """
    data = list()
    if deps.validate_key(model_id):
        model = crud.traintask.get_by_key_owner(
            db=db,
            owner_id=curr_user.id,
            key=model_id,
        )
        if model is None:
            return JSONResponse(
                status_code=status.HTTP_404_NOT_FOUND,
                content={'msg': f"A model with id:{model_id} is not exists"}
            )
        data.append(ModelReport(
            task_id=model.task_id,
            model_id=model.model_id,
            labels=model.labeldict,
            state=model.state,
            meta=model.meta).todict
        )
    else:
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail="Invalid Key"
        )

    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=data
    )

@router.delete("/facial/models/{model_id}", tags=['facial-recog'])
def delete_model(
    model_id: str,
    db: Session = Depends(deps.get_db),
    curr_user: models.User = Depends(deps.get_current_active_user),
):
    """
    Delete user's model by model-id.
    """
    if deps.validate_key(model_id):
        model = crud.traintask.delete_by_key_owner(
            db=db,
            owner_id=curr_user.id,
            key=model_id
        )
    else:
        raise HTTPException(
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        detail="Invalid Key"
    )
    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=model
    )


@router.post("/facial/train_facialrecog", tags=['facial-recog'])
async def train_face(
        db: Session = Depends(deps.get_db),
        curr_user: models.User = Depends(deps.get_current_active_user),
        labels: List[str] = Form(...),
):
    """
    Learn new faces from user uploaded video.
    Accept Form/text containing labels needed to make the model.
    Error if no such any label does not have a uploaded video folder.
    Description:
    train_face will generate modelkey as a key for accessing the model later,
    and passing the key to celery task.
    train_face also handle creating task in database -> celery worker can handles too but not implemented. # noqa
    after launch task, train_face will return celery's task id for acquiring the status later.
    or maybe just query the database, and let's worker update to the database instead.
    """
    _labels = []
    # get the label, user can wait lol.
    for label in labels:
        for _label in label.split(','):
            _labels.append(_label)
    usersourceDir = Path(curr_user.storage)\
        .joinpath(settings.USER_UPLOAD_DIR)\
        .joinpath(settings.USER_VIDEO_DIRNAME)
    logger.debug(f"Training with label specified: {_labels}")
    key = str(uuid4())
    res = train_facial_recognition.delay(
            key=key,
            source_video_storage=str(usersourceDir),
            labels=_labels,
            output_path=curr_user.storage,
        )
    inspath = Path(curr_user.storage).joinpath(face_recog_pattern(key))  # storage/userStorage/username/inspath/
    modelname = face_recog_model_template(key)  # repr-{}.pkl
    modeldir = inspath.joinpath('Model')
    modelpath = modeldir.joinpath(modelname)
    task_state = 'INQUEUE'
    scheme = schemas.TrainTaskCreate(
        task_id=res.id,
        model_id=key,
        state=task_state,
        meta='Task created',
        instancepath=str(inspath),
        modelpath=str(modelpath),
        labels=json.dumps(_labels)
    )
    crud.traintask.create_with_owner(
        db=db,
        obj_in=scheme,
        owner_id=curr_user.id
        )
    return JSONResponse(
        status_code=status.HTTP_202_ACCEPTED,
        content={
                    'task_id': res.id,
                    'model_id': key,
                 },
    )


# Train task progress tracking
@router.get("/facial/train/progress")
async def train_task_progress(
    task_id: str,
    db: Session = Depends(deps.get_db),
    curr_user: models.User = Depends(deps.get_current_active_user),
) -> TaskProgress:
    task = crud.traintask.get_by_taskid_owner(
        db=db, owner_id=curr_user.id, key=task_id)
    if task:
        if task.state == 'INQUEUE':
            result = AsyncResult(task_id)
            if result.state == 'PENDING':
                # Task mostly likely failed.
                #  Because callback was not activated.
                ret = TaskProgress(state='FAILURE', info={'msg': 'UNKNOWN-NO_CALLBACK'})
            ret = TaskProgress(state=result.state, info=result.info)
        else:
            ret = TaskProgress(state=task.state, info={'msg': 'EXECUTED'})
        return ret
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail='Task cannot be found'
        )
