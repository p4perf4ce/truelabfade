# Event Handler
import os
from typing import Callable
from pathlib import Path

from loguru import logger
from fastapi import FastAPI

# Model Handlers
from app.core.config import settings


def startup_handler(app: FastAPI) -> Callable:
    async def start() -> None:
        # Do startup task
        logger.info("Starting up API server ...")
        logger.info("Storage ... check")
        # Check model availability
        if not os.path.isfile(settings.XML_FACE_CASCADE):
            logger.info(
                f"Face detection cascade not found at {settings.XML_FACE_CASCADE}")
            raise FileNotFoundError
        if not os.path.isfile(settings.FACE_POS_WEIGHT):
            logger.info(
                f"Face model weight not found at {settings.FACE_POS_WEIGHT}")
            raise FileNotFoundError
        if not os.path.isfile(settings.FACE_POS_DEPLOY_TXT):
            logger.info(
                f"Face model structure not found at {settings.FACE_POS_DEPLOY_TXT}")
            raise FileNotFoundError
        if not os.path.isfile(settings.FACENET_WEIGHT):
            logger.info(
                f"Face-Recog model storage not found at {settings.FACENET_WEIGHT}")
            raise FileNotFoundError
        if not os.path.isfile(settings.AGE_MODEL):
            logger.info(
                f"Age prediction model weight not found at {settings.AGE_MODEL}")
            raise FileNotFoundError
        if not os.path.isfile(settings.EMOTION_MODEL):
            logger.info(
                f"Emotion detection model weight not found at {settings.EMOTION_MODEL}")
            raise FileNotFoundError
        if not os.path.isfile(settings.GENDER_MODEL):
            logger.info(
                f"Gender detection model weight not found at {settings.GENDER_MODEL}")
            raise FileNotFoundError
        if not os.path.isfile(settings.FACE_LANDMARK_MODEL):
            logger.info("Face landmark model not found at {setting.FACE_LANDMARK_MODEL}")
            raise FileNotFoundError
        logger.info("Model availability ... check")
    return start


def shutdown_handler(app: FastAPI) -> Callable:
    async def shutdown() -> None:
        # Do pre-shutdown task
        logger.info("Shutting down ...")
        # _drop_model(app)
    return shutdown
