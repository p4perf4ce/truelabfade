# application configuration
from enum import Enum
from typing import Optional, Dict, Any

from starlette.config import Config
from starlette.datastructures import Secret

from pydantic import (
    AnyHttpUrl, BaseSettings, HttpUrl, validator,
    PostgresDsn, EmailStr)


class Settings(BaseSettings):

    # Load Starlette Configuration
    # Read from environment variable
    config = Config('.env')

    # API CONFIGURATION

    APP_VERSION: str = config('APP_VERSION')
    APP_NAME: str = config('APP_NAME')
    DEBUG: bool = config('DEBUG', cast=bool, default=False)

    # Intent for local use
    CLIENT_ID: str = config('CLIENT_ID')
    CLIENT_SECRET: str = config('CLIENT_SECRET')
    FIRST_SUPERUSER: str = config('FIRST_SUPERUSER')
    FIRST_SUPERUSER_PASSWORD: str = config('FIRST_SUPERUSER_PASSWORD')
    FIRST_SUPERUSER_EMAIL: EmailStr = config('FIRST_SUPERUSER_EMAIL')

    # Bases URL

    APP_BASE_URL: str = config('API_BASE_URL', default="http://localhost")

    # Security
    SECRET_KEY: str = config('SECRET_KEY')
    AUTH_BASE_STR: str = config('AUTH_BASE_STR', default='/auth')
    AUTHORIZE_STR: str = config('AUTHORIZE_STR',
                                default='/auth.oauth2')
    ACCESS_TOKEN_STR: str = config('ACCESS_TOKEN_STR',
                                   default='/token.oauth2')
    CHECK_TOKEN_STR: str = config('CHECK_TOKEN_STR',
                                  default='/check.oauth2')
    PASS_RESET_VIA_EMAIL_STR: str = config('PASS_RESET_VIA_EMAIL_TR',
                                           default='/resetpass')
    PASS_RESET_VIA_CLIENT_STR: str = config('PASS_RESET_STR',
                                            default='/resetpaas.client')
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 30  # 30 minutes
    SCOPES = {"profile": "profile", "openid": "openid"}

    # Database

    POSTGRES_SERVER: str = config('POSTGRES_SERVER')
    POSTGRES_USER: str = config('POSTGRES_USER')
    POSTGRES_PASSWORD: str = config('POSTGRES_PASSWORD')
    POSTGRES_DB: str = config('POSTGRES_DB')
    SQLALCHEMY_DATABASE_URI: Optional[PostgresDsn] = None

    # Storage Configuration
    STORAGE_PATH: str = config('STORAGE_PATH')
    STATIC_MODEL_STORAGE: str = config('STATIC_MODEL_STORAGE')
    USER_STORAGE: str = config('USER_STORAGE')
    USER_UPLOAD_DIR: str = config('USER_UPLOAD_DIR')
    USER_IMAGE_DIRNAME: str = config('USER_IMAGE_DIRNAME')
    USER_VIDEO_DIRNAME: str = config('USER_VIDEO_DIRNAME')
    USER_MODEL_DIRNAME: str = config('USER_MODEL_DIRNAME')

    IMAGE_PREFIX: str = config('IMAGE_PREFIX')
    VIDEO_PREFIX: str = config('VIDEO_PREFIX')

    # Model Configuration
    XML_FACE_CASCADE: str = config('XML_FACE_CASCADE')
    AGE_MODEL: str = config('AGE_MODEL')
    GENDER_MODEL: str = config('GENDER_MODEL')
    EMOTION_MODEL: str = config('EMOTION_MODEL')
    FACE_POS_DEPLOY_TXT: str = config('FACE_POS_DEPLOY_TXT')
    FACE_POS_WEIGHT: str = config('FACE_POS_WEIGHT')
    FACENET_WEIGHT: str = config('FACENET_WEIGHT')
    FACE_LANDMARK_MODEL: str = config('FACE_LANDMARK_MODEL')

    @validator("SQLALCHEMY_DATABASE_URI", pre=True)
    def assemble_db_connection(cls, v: Optional[str], values: Dict[str, Any]) -> Any:
        if isinstance(v, str):
            return v
        return PostgresDsn.build(
            scheme="postgresql",
            user=values.get("POSTGRES_USER"),
            password=values.get("POSTGRES_PASSWORD"),
            host=values.get("POSTGRES_SERVER"),
            path=f"/{values.get('POSTGRES_DB') or ''}",
        )

    class Config:
        case_sensitive = True


settings = Settings()


# Enumerator

# Upload Configuation
class AllowedType(Enum):
    @classmethod
    def has(cls, value):
        return value in cls._value2member_map_


class AllowImageType(str, AllowedType):
    jpg = 'jpg'
    jpeg = 'jpeg'
    png = 'png'
    dotjpg = '.jpg'
    dotjpeg = '.jpeg'
    dotpng = '.png'


class AllowVideoType(str, AllowedType):
    mp4 = 'mp4'
    dotmp4 = '.mp4'
