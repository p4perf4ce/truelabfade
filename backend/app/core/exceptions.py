# Exceptions

class UnallowedFormat(Exception):
    """
    Unallowed uploaded file format
    """
    def __init__(self, ext: str):
        self.ext = ext


class LabelExists(Exception):
    """
    Duplicate label uploaded.
    """
    def __init__(self, label: str):
        self.label = label


class UploadFailure(Exception):
    """
    Assertion error in upload procedure
    """
    def __init__(self, label: str):
        self.label = label
