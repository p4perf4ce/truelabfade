
from pydantic import BaseModel


class UploadVideoResp(BaseModel):
    label: str
    total: int
