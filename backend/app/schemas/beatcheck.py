from pydantic import BaseModel


class Beat(BaseModel):
    """
    Server heartbeat status
    """
    is_alive: bool
