from typing import List, Dict

from pydantic import BaseModel


class JobModel(BaseModel):
    job: str


class FaceRecog(JobModel):
    """
    Face recognition result
    """
    job: str = 'face-recognition'
    faces_name: Dict
    detail: Dict


class AgePrediction(JobModel):
    """
    Age prediction result
    """
    job: str = 'age-prediction'
    age: List
    detail: Dict


class GenderDetection(JobModel):
    """
    Gender detection result
    """
    job: str = 'gender-detection'
    gender: List
    detail: Dict


class EmotionDetection(JobModel):
    """
    Emotion detection result
    """
    job: str = 'emotion-detection'
    emotion: List
    detail: Dict
