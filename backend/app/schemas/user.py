from typing import Optional

from pydantic import BaseModel, EmailStr


# Factory
class UserBase(BaseModel):
    username: str = None  # AttachValidator Later
    email: Optional[EmailStr] = None
    is_active: Optional[bool] = True
    is_superuser: bool = False
    full_name: Optional[str] = None
    storage: Optional[str] = None


# Create user via API
class UserCreate(UserBase):
    username: str
    email: EmailStr
    password: str


# Update user
class UserUpdate(UserBase):
    email: Optional[EmailStr] = None
    password: Optional[str] = None


class UserInDBBase(UserBase):
    id: Optional[int] = None

    class Config:
        orm_mode = True


# Return properties via API
class User(UserInDBBase):
    pass


# Additional properties to store in DB
class UserInDB(UserInDBBase):
    hashed_password: str
