from typing import Any

from pydantic import BaseModel


class TaskProgress(BaseModel):
    state: str
    info: Any
