# API Responses Schema

# CRUD Schema
from .item import Item, ItemCreate, ItemInDB, ItemUpdate  # noqa
from .token import Token, TokenPayload  # noqa
from .user import User, UserCreate, UserInDB, UserUpdate  # noqa
from .train_task import TrainTask, TrainTaskCreate, TrainTaskInDB, TrainTaskUpdate  # noqa

# Generic Schema
from .beatcheck import Beat  # noqa: F401
from .prediction import (  # noqa: F401
    FaceRecog, AgePrediction,
    EmotionDetection, GenderDetection)
from .upload import UploadVideoResp  # noqa: F401
from .msg import Msg  # noqa
from .taskprogress import TaskProgress  # noqa
from .modelreport import ModelReport  # noqa: F401
