from typing import Any
from pydantic import BaseModel


class ModelReport(BaseModel):
    task_id: str
    model_id: str
    labels: Any
    state: str
    meta: Any
    @property
    def todict(self):
        return {
                'task_id': self.task_id,
                'model_id': self.model_id,
                'labels': self.labels,
                'state': self.state,
                'meta': self.meta,
            }
