from typing import Optional

from pydantic import BaseModel


# Factory

class ItemBase(BaseModel):
    # Upload to predict - No name,
    # shouldn't create in first place though.
    # Upload to train - name as label
    name: Optional[str] = None
    item_type: Optional[str] = None
    path: Optional[str] = None


# Create Item
class ItemCreate(ItemBase):
    name: str = None
    item_type: str = None
    path: str = None


# Update
class ItemUpdate(ItemBase):
    pass


class ItemInDBBase(ItemBase):
    id: int
    name: str
    path: str
    owner_id: int

    class Config:
        orm_mode = True


# Return properties
class Item(ItemInDBBase):
    pass


# Properties of properties in DB
class ItemInDB(ItemInDBBase):
    pass
