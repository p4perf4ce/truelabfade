from typing import Optional

from pydantic import BaseModel


# Factory

class TrainTaskBase(BaseModel):
    # Upload to predict - No name,
    # shouldn't create in first place though.
    # Upload to train - name as label
    task_id: Optional[str] = None
    state: Optional[str] = None
    meta: Optional[str] = None
    instancepath: Optional[str] = None
    modelpath: Optional[str] = None
    labels: Optional[str] = None


# Create Item
class TrainTaskCreate(TrainTaskBase):
    task_id: str
    model_id: str
    state: str
    meta: str
    instancepath: str
    modelpath: str
    labels: str


# Update
class TrainTaskUpdate(TrainTaskBase):
    pass


class TrainTaskInDBBase(TrainTaskBase):
    id: int
    task_id: str
    model_id: str
    owner_id: int

    class Config:
        orm_mode = True


# Return properties
class TrainTask(TrainTaskInDBBase):
    pass


# Properties of properties in DB
class TrainTaskInDB(TrainTaskInDBBase):
    pass
