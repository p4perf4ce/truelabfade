#! /bin/bash
source activate fade

# Let the DB start
echo "Start Database Session ..."
python /app/app/backend_prestart.py

# Run migrations
echo "Running alembic migration ..."

# Check alembic
ALEMBIC_VER=${ALEMBIC_VER_PATH:-/app/alembic/versions}
echo "Checking for alembic/versions directory in $ALEMBIC_VER"
if [ -d $ALEMBIC_VER ] ; then
    echo "$ALEMBIC_VER exists."
else
    echo "Creating $ALEMBIC_VER"
    mkdir $ALEMBIC_VER
fi

echo "Checking exists any versions of migration exists in $ALEMBIC_VER"
if [ ! $(ls -A $ALEMBIC_VER | grep -i ".py") ] ; then
    echo "No versions file existed. Running first migration ..."
    alembic revision --autogenerate -m "auto first migration"
else
    num=`expr $(ls -1 | grep ".ini" | wc -l) + 1` 
    echo "Versions file exists. Running $num# migrations."
    alembic revision --autogenerate -m "auto $num migration"
fi

echo "Upgrading head ..."
alembic upgrade head

# Create initial data in DB
echo "Initializing data"
python /app/app/initial_data.py
