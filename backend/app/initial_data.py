import os
import logging

from app.db.init_db import init_db
from app.db.session import SessionLocal
from app.core.config import settings

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def _storage_check(storage_path: str,
                   upload_path: str,
                   ) -> None:
    if not os.path.isdir(storage_path):
        logger.debug("Creating storage dir at {}"
                     .format(storage_path))
        os.mkdir(storage_path)
    if not os.path.isdir(upload_path):
        logger.debug("Creating user upload dir at {}"
                     .format(upload_path))
        os.mkdir(upload_path)


def init() -> None:
    db = SessionLocal()
    init_db(db)


def main() -> None:
    logger.info("Checking storage availability")
    _storage_check(settings.STORAGE_PATH, settings.USER_STORAGE)
    logger.info("Storage Availability ... Checked")
    logger.info("Creating initial data")
    init()
    logger.info("Initial data created")

if __name__ == "__main__":
    main()
